========================
pytest-report-parameters
========================

pytest plugin for adding tests' parameters to junit report.


Install
-------
Install this plugin::

    $ pip install pytest-report-parameters
